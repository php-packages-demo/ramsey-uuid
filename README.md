# ramsey/uuid

Generating RFC 4122 version 1, 3, 4, and 5 universally unique identifiers (UUID). https://packagist.org/packages/ramsey/uuid

[![PHPPackages Rank](http://phppackages.org/p/ramsey/uuid/badge/rank.svg)](http://phppackages.org/p/ramsey/uuid)
[![PHPPackages Referenced By](http://phppackages.org/p/ramsey/uuid/badge/referenced-by.svg)](http://phppackages.org/p/ramsey/uuid)
